#!/bin/bash
. /usr/lib/ckan/default/bin/activate
cd /usr/lib/ckan/default/src/ckan
VAR=$(expect -c "spawn paster --plugin=pylons shell /etc/ckan/default/production.ini
    expect >>> { 
        send \"model.User.get('${1}').purge()\r\" 
        sleep 1
        send \"model.Session.commit()\r\" 
        sleep 1
        send \"model.Session.remove()\r\" 
        sleep 1
        send \"quit()\r\" 
    }
")