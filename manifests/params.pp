# == Class: ckan::params
#
# The os specific parameters for ckan installs.
#
# === Parameters
#
# === Variables
#
# === Copyright
#
# GPL-3.0+
#
class ckan::params {

  # variables
  $site_url               = 'localhost'
  $site_title             = 'localhost'
  $site_description       = ''
  $site_intro             = ''
  $site_about             = ''
  $plugins                = ''
  $app_instance_id        = ''
  $beaker_secret          = ''
  $site_logo              = ''
  # due to issue, have to supply https://github.com/ckan/ckan/issues/2110
  # our own license file
  $license                = 'puppet:///modules/ckan/license.json'
  $is_ckan_from_repo      = true
  $ckan_version           = undef
  $ckan_package_url       = undef
  $ckan_package_filename  = undef
  $custom_css             = 'main.css'
  $custom_imgs            = ''
  $recaptcha_version      = '2'
  $recaptcha_publickey    = undef
  $recaptcha_privatekey   = undef
  $max_resource_size      = 100
  $max_image_size         = 10
  $datapusher_formats     = 'csv xls application/csv application/vnd.ms-excel'
  $default_views          = 'image_view recline_view'
  $preview_loadable       =
    "html htm rdf+xml owl+xml xml n3 n-triples turtle plain atom csv\
 tsv rss txt json"
  $text_formats           = 'text plain text/plain'
  $json_formats           = 'json'
  $xml_formats            = 'xml rdf rss'
  $postgres_pass          = 'pass'
  $ckan_pass              = 'pass'
  $pg_hba_conf_defaults   = true
  $pg_hba_rules           = undef
  $postgis_version        = '2.1'
  $install_ckanapi        = false
  $enable_backup          = true
  $backup_daily           = true
  $backup_dir             = '/backup'
  $solr_url               =
  'http://apache.mirrors.lucidnetworks.net/lucene/solr'
  $solr_version           = '5.3.0'
  $solr_schema_version    = 'default'
  $locale_default         = 'en'
  $i18n_directory         = ''
  $display_timezone       = 'UTC'

  # atom feeds
  $feeds_author_name      = ''
  $feeds_author_link      = ''
  $feeds_authority_name   = ''
  $feeds_date             = ''

  # smtp mail setttings
  $smtp_server            = undef
  $smtp_starttls          = undef
  $smtp_user              = undef
  $smtp_password          = undef
  $smtp_mail_from         = undef
  $email_errors_from      = undef
  $email_errors_to        = undef

  # additional variables imported from config
  $ckan_etc          = '/etc/ckan'
  $ckan_default      = "${ckan_etc}/default"
  $ckan_src          = '/usr/lib/ckan/default/src/ckan'
  $ckan_ext          = '/usr/lib/ckan/default/src'
  $ckan_img_dir      = "${ckan_src}/ckan/public/base/images"
  $ckan_css_dir      = "${ckan_src}/ckan/public/base/css"
  $ckan_storage_path = '/var/lib/ckan/default'
  $ckan_plugin_dir   = "${ckan_etc}/plugins"
  $license_file      = 'license.json'
  $ckan_conf         = "${ckan_default}/production.ini"
  $paster            = '/usr/lib/ckan/default/bin/paster'

  # used for setting jts for spatial search
  $jts_base_url =
  'http://search.maven.org/remotecontent?filepath=com/vividsolutions/jts/'
  $jts_1_13  = "${jts_base_url}/1.13/jts-1.13.jar"
  $jts_1_12  = "${jts_base_url}/1.12/jts-1.12.jar"
  $jts_1_11  = "${jts_base_url}/1.11/jts-1.11.jar"
  $jts_url   = $jts_1_13

  # OS Specific configuration
  case $::osfamily {

      'redhat': {
        $required_packages = ['epel-release','httpd', 'mod-wsgi', 'nginx',
                              'libpqxx-devel','python-paste-script', 'expect']
        # wsgi specific configuration
        $wsgi_command = '/usr/sbin/a2enmod wsgi'
        $wsgi_creates = '/etc/httpd/mods-enabled/wsgi.conf'
        $apache_server = 'httpd'
      }

      'debian':{
        $required_packages = ['apache2', 'libapache2-mod-wsgi', 'nginx',
                              'libpq5', 'python-pastescript', 'expect',
                              'python2.7', 'python-pip']
        # wsgi specific configuration
        $wsgi_command = '/usr/sbin/a2enmod wsgi'
        $wsgi_creates = '/etc/apache2/mods-enabled/wsgi.conf'
        $apache_service = 'apache2'
      }

      default: {
        fail("Unsupported OS ${::osfamily}.  Please use a debian or \
redhat based system")
      }
  }

  # the path to the python version packaged with ckan
  $python = '/usr/lib/ckan/default/bin/python'

  # Ubuntu specific
  if $::operatingsystem == 'Ubuntu' and $::lsbdistrelease == '14.04' {
    $pip = '/usr/lib/ckan/default/bin/pip'
  }else {
    $pip = '/usr/bin/pip'
  }
}
