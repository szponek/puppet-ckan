# == Class: ckan::ext::datastore
#
# Configures the datastore extension as its built-in.
#
# See the plugin documentation for full details:
#
#  http://docs.ckan.org/en/latest/maintaining/datastore.html
#
# == Notes ==
#
# Cannot use the ckan::ext class as datastore is installed by default.
#
# By default, the configuration for postgresql is already handled,
# so there is no need to specify it in this module.
#
class ckan::ext::datastore {
  anchor {'ckan::ext::datastore::begin':}

  file {"${ckan::ckan_plugin_dir}/datastore":
    content => 'datastore',
    notify  => Exec['update_plugins'],
  }

  anchor {'ckan::ext::datastore::end':
    require => File["${ckan::ckan_plugin_dir}/datastore"],
  }
}
