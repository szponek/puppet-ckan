# == Class: ckan::ext::geoview
#
# Installs the geoview extension.
#
# See the plugin documentation for full details:
#
#   https://github.com/ckan/ckanext-geoview
#
# == Requirements ==
#   In order for this module to update the default_views parameter,
#   it MUST be declared before the ckan module in the manifests.
#   This will allow all of the variables to be evaulated.
#
#   Also, ensure that resource_proxy plugin has been enabled.
#
# == Parameters ==
#
#  [*openlayers_formats*]
#    A string seperated by spaces.
#    Available formats
#    - Web Map Service (WMS)      wms
#    - Web Feature Service (WFS)  wfs
#    - GeoJSON                    geojson
#    - GML                        gml
#    - KML                        kml
#    - ArcGIS REST API            arcgis_rest
#    - Google Fusion Tables       gft (requires google_api_key)
#    Default: 'wms geojson'
#
#  [*openlayers_hide_overlays*]
#    Overlays won't be visible by default (only the base layer)
#    Default: false
#
#  [*openlayers_hoveron*]
#    The feature data popup will be displayed when hovering on.
#    Default: true
#
#  [*openlayers_google_api_key*]
#    The google api key required to render google fusion table resources.
#    See https://developers.google.com/fusiontables/docs/v1/using#APIKey
#    Default: undef
#
class ckan::ext::geoview (
  $openlayers_viewer_enable      = true,
  $openlayers_formats            = 'wms geojson',
  $openlayers_hide_overlayes     = false,
  $openlayers_hoveron            = true,
  $openlayers_google_api_key     = undef,
  $leaflet_geojson_viewer_enable = true,
  $leaflet_wmts_viewer_enable    = true,
){
  anchor {'ckan::ext::geoview::begin':
    #require => Class['ckan::config'],
    before  => Class['ckan::postconfig']
  }

  ## == openlayers viewer == ##
  if $openlayers_viewer_enable {
    # set the view
    $openlayers_view   = 'geo_view'
    $openlayers_plugin = 'geo_view'

    # add additional configuration to main config file
    concat::fragment { 'ckanext-geoview-openlayers':
      target  => '/etc/ckan/default/production.ini',
      content => "
ckanext.geoview.ol_viewer.formats =                 ${openlayers_formats}
ckanext.geoview.gapi_key =                          ${openlayers_google_api_key}
ckanext.geoview.ol_viewer.hide_overlays =           ${openlayers_hide_overlayes}
ckanext.geoview.ol_viewer.default_feature_hoveron = ${openlayers_hoveron}
",
      order   => 03, #Right after the head
    }
  }else{
    $openlayers_view   = undef
    $openlayers_plugin = undef
  }

  ## == geojson == ##
  if $leaflet_geojson_viewer_enable {
    $geojson_view   = 'geojson_view'
    $geojson_plugin = 'geojson_view'
  }else{
    $geojson_view   = undef
    $geojson_plugin = undef
  }

  ## == wmts == ##
  if $leaflet_wmts_viewer_enable {
    $wmts_view   = 'wmts_view'
    $wmts_plugin = 'wmts_view'
  }else{
    $wmts_view   = undef
    $wmts_plugin = undef
  }

  # collect variables
  $default_views   = join([$openlayers_view,$geojson_view,$wmts_view],' ')
  $plugins         = join([$openlayers_plugin,$geojson_plugin,$wmts_plugin],' ')

  ckan::ext { 'geoview':
    plugin   => $plugins,
    revision => 'v0.0.10',
    require  => Anchor['ckan::ext::geoview::begin'],
  }

  anchor {'ckan::ext::geoview::end':
    require => Ckan::Ext['geoview'],
  }
}
