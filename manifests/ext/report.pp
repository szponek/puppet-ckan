# == Class: ckan::ext::report
#
# Installs the report extension.
#
# See the plugin documentation for full details:
#
#   https://github.com/datagovuk/ckanext-report
#
class ckan::ext::report (
  $ckan_conf = $ckan::params::ckan_conf,
  $paster = $ckan::params::paster,
) {

  anchor {'ckan::ext::report::begin':}

  ckan::ext { 'report':
    plugin   => 'report',
    source   => 'git://github.com/datagovuk/ckanext-report.git',
    revision => 'master',
    require  => Anchor['ckan::ext::report::begin'],
  }

  check_run::task { 'install-ckan-report-init':
    exec_command =>
"${ckan::paster} --plugin=ckanext-report report initdb --config=${ckan_conf}",
    require      => Ckan::Ext['report'],
  }

  anchor {'ckan::ext::report::end':
    require => Check_run::Task['install-ckan-report-init'],
  }
}
