# == Class: ckan::ext::harvest
#
# Installs the harvest extension.
#
# See the plugin documentation for full details:
#
#   https://github.com/ckan/ckanext-harvest
#
# == Parameters ==
#
# [*ckan_conf*]
#   Default: $ckan::params::ckan_conf
#
# [*paster*]
#   Default: $ckan::params::paster
#
class ckan::ext::harvest (
  $ckan_conf = $ckan::params::ckan_conf,
  $paster    = $ckan::params::paster,
) {

  anchor {'ckan::ext::harvest::begin':}

  include ckan::redis
  ckan::ext { 'harvest':
    plugin   => 'harvest ckan_harvester',
    revision => 'master',
    require  => [Anchor['ckan::ext::harvest::begin'],Class['ckan::redis']],
  }

  check_run::task { 'install-ckan-harvest-init':
    exec_command =>
"${ckan::paster} --plugin=ckanext-harvest harvester init --config=${ckan_conf}",
    require      => Ckan::Ext['harvest'],
  }

  concat::fragment { 'ckan_ext_harvest_redis_config':
    target => $ckan::ckan_conf,
    source => 'puppet:///modules/ckan/config/harvest.conf.snippet',
    order  => 02, #Right after the head
  }

  #Two unlisted requirements.... naughty devs
  ckan::pip_package { 'mock':
    require => Check_run::Task['install-ckan-harvest-init'],
  }

  #And it needs some background jobs running, because $REASONS
  #Using supervisor to do this isn't terrible
  include ckan::supervisor

  ckan::supervisor::program { 'harvester_gather_consumer':
    command => "/usr/lib/ckan/default/bin/paster --plugin=ckanext-harvest\
 harvester gather_consumer --config=${ckan::ckan_conf}",
    require => Ckan::Pip_package['mock'],
  }
  ckan::supervisor::program { 'harvester_fetch_consumer':
    command =>"/usr/lib/ckan/default/bin/paster --plugin=ckanext-harvest\
 harvester fetch_consumer --config=${ckan::ckan_conf}",
    require => Ckan::Supervisor::Program['harvester_gather_consumer'],
  }

  anchor {'ckan::ext::harvest::end':
    require => Ckan::Supervisor::Program['harvester_fetch_consumer'],
  }
}
