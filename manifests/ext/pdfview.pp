# == Class: ckan::ext::pdfview
#
# Installs the "pdf_view" extension
# Note: if you want to enable pdf views of documents outside of
#       the ckan instance, you will need to enable resource_proxy plugin.
#
# See the plugin documentation for full details:
#
#   https://github.com/ckan/ckanext-pdfview
#
class ckan::ext::pdfview {
  ckan::ext { 'pdfview':
    source   => 'git://github.com/ckan/ckanext-pdfview.git',
    revision => 'master',
    plugin   => 'pdf_view',
  }
}