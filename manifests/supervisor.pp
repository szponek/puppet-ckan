# == Class: ckan::supervisor
#
# Very simple (barely there) installation of supervisor
# This is used to run/manage background permanent tasks,
# e.g. for harvester jobs etc.
#
# See also ckan::supervisor::program for deploying programs run by supervisor
#
# === Examples
#
# include ckan::supervisor
# And some use of ckan::supervisor::program (see that define for details)
#
# === Copyright
#
# GPL-3.0+
#
class ckan::supervisor {
  anchor{'ckan::supervisor::begin':}

  package { 'supervisor':
    ensure  => 'present',
    require => Anchor['ckan::supervisor::begin'],
  }

  service { 'supervisor':
    ensure  => running,
    require => Package['supervisor'],
  }

  exec { 'supervisor-update':
    command     => '/usr/bin/supervisorctl update',
    refreshonly => true,
    require     => Service['supervisor'],
  }

  anchor{'ckan::supervisor::end':
    require => Exec['supervisor-update'],
  }
}
