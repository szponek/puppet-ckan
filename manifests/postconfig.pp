# == Class ckan::postconfig
#
#
class ckan::postconfig {

  anchor{'ckan::postconfig::begin':}

  # gather variables from extensions
  # == default_views == ##
  if $ckan::ext::geoview::default_views {
    $geoview_default_views = $ckan::ext::geoview::default_views
  }else{
    $geoview_default_views = ''
  }
  $default_views = join([$ckan::default_views, $geoview_default_views],' ')

  concat::fragment { 'config_default_views':
    target  => $ckan::ckan_conf,
    content => template('ckan/production_default_views.erb'),
    order   => 02,
  }

  anchor{'ckan::postconfig::end':
  }
}
