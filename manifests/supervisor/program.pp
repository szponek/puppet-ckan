# == Type: ckan::supervisor::program
#
# Adds a 'program' (job/task) managed by supervisor
#
# === Parameters
#
# [*name*] (or title)
#   Arbitrary string, will be the name of the 'program' in supervisor, i.e.
#    the name given to supervisorctl (start|stop|retart|status) <name>
#
# [*command*]
#   The fully path/arguments of the command that is the program to run
#
# === Examples
#
#  include ckan::supervisor
#  ckan::supervisor::program { 'harvester_fetch_consumer':
#    command => "/usr/lib/ckan/default/bin/paster\
# --plugin=ckanext-harvest harvester fetch_consumer\
# --config=${ckan::ckan_conf}",
#  }
#
# === Copyright
#
# GPL-3.0+
#
define ckan::supervisor::program($command) {
  anchor{"ckan::supervisor::program::${title}::begin":}

  file { "/etc/supervisor/conf.d/${name}.conf":
    content => template('ckan/etc/supervisor/conf.d/program.conf.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    notify  => Exec['supervisor-update'],
    require => [Package['supervisor'],
              Anchor["ckan::supervisor::program::${title}::begin"]],
  }

  anchor{"ckan::supervisor::program::${title}::end":
    require => File["/etc/supervisor/conf.d/${name}.conf"],
  }
}
